@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Home</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    Bienvenido estimado {{ Auth::user()->usuario }}, este sistema web le permite 
                    controlar  los requerimientos en sus proyectos. ¿Desea invitar amigos? ¿Revisar 
                    el progreso de su proyecto? ¿Administrar varios proyectos? Todo estas funcionalidades 
                    y mas estan a su disposición.
                </div>

                {{--<div class="col-sm-5 col-md-5 col-lg-5 pull-right">
                    <div class="well well-lg" >
                        Proyectos 
                    </div>
                </div>
                --}}
            </div>
        </div>
    </div>
</div>
@endsection
