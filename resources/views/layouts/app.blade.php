<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Improve') }}</title>


   
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>




    <link rel="stylesheet" href="{{ asset ('font-awesome-5/css/fontawesome-all.min.css')}}">
    <!-- https://fontawesome.com/ -->
{{--    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
    --}}


    <!-- https://getbootstrap.com/ -->
    <link rel="stylesheet" href="{{asset ('css/templatemo-style.css')}}">
    <style>

        #celda {
        color:white;
        text-align:center;
        }
    </style>

</head>

  <nav class="navbar navbar-inverse">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">Gesti</a>
      </div>
      <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav">
            {{--<li class="nav-item">
                <a class="nav-link" href="{{ url('/home') }}">
                <i class="fa fa-home" aria-hidden="true"></i>Home</a>
            </li>--}}
        </ul>
        <ul class="nav navbar-nav navbar-right">
            @guest
                <li class="nav-item">
                    <a  class="nav-link" href="{{ route('login') }}">
                    <i class="fa fa-sign-in-alt" aria-hidden="true"></i> Login</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('register') }}">
                    <i class="fa fa-user-plus" aria-hidden="true"></i> Registro</a>
                </li>
            @else
                @if(Auth::user()->role_id == 1) 
                    <li class="nav-item">
                        <a class="nav-link" href="/admin/proyectos">
                        <i class="fa fa-briefcase" aria-hidden="true"></i> Todos los Proyectos</a>
                    </li>
                    
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('users.index') }}">
                        <i class="fa fa-user" aria-hidden="true"></i> Todos los Usuarios</a>
                    </li>
                @endif  
                
            <li class="nav-item">
                    <a class="nav-link" href="{{ url('/home') }}">
                    <i class="fa fa-home" aria-hidden="true"></i>Home</a>
                </li>
            <li class="nav-item">
                    <a class="nav-link" href="{{ route('projects.index') }}">
                    <i class="fa fa-briefcase" aria-hidden="true"></i> Proyectos</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/usuarios/{{ Auth::user()->id }}/editar">
                <i class="fa fa-user" aria-hidden="true"></i> Perfil</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                    <i class="fa fa-power-off" aria-hidden="true"></i> Logout
                </a>
                <form id="logout-form" action="{{ route('logout') }}" 
                    method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>        
            </li>
            @endif
        </ul>
      </div>
    </div>
  </nav> 
 


<body>
    
    <div id="app">
        
      

        <div class="container" >
            @include('partials.errors')
            @include('partials.success')
            @yield('content')
        </div>

    </div>

    <footer class="tm-footer row tm-mt-small">
        <div class="col-12 font-weight-light">
            <p class="text-center text-white mb-0 px-4 small"> 
             Copyright © PROYECTO DE GRADO <b>II-2019</b>
            </p>
        </div>
    </footer>

    <script src="{{ asset('js/app.js') }}"></script>

    @yield('jqueryScript')
</body>
</html>
