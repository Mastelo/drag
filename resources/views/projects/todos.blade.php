@extends('layouts.app')

@section('content')


<div class="row tm-content-row">
    <div class="col-md-12">
        <h1 align=center>
            {{ $num_proyects }} Proyecto(s) registrado(s) 
        </h1>
        @if($num_proyects>0)
        <table width="100" class="table table-striped table-hover table-reflow">
            <thead>
                <tr>
                    <th ><strong> ID </strong></th>
                    <th ><strong> Proyecto </strong></th>
                    <th ><strong> Descripción </strong></th>
                    <th ><strong> Eliminar </strong></th>
                    
                </tr>
            </thead>
            <tbody>
                @foreach($proyectos as $proyecto)
                    <tr>
                        <td> {{$proyecto->id}}</td>
                        <td>
                            <a href="/projects/{{ $proyecto->id }}" > {{ $proyecto->name }}</a>
                        </td>
                        <td> {{$proyecto->description}}</td>
                        <td>
                            <a  
                                onclick="return confirm('Esta accion eliminara todo el proyecto')"
                                href="/admin/{{ $proyecto->id }}/eliminar" 
                                class="btn btn-danger btn-sm">
                                <i class="fa fa-times" aria-hidden="true"></i> 
                            </a> 
                        </td>
                        
                    </tr> 
                @endforeach
            </tbody>
        </table>
        @endif

        
    </div>
</div>

@endsection
